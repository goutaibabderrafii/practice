import React, { useState } from 'react';
import { Button, Input, Form } from 'antd';
import {
  UserAddOutlined,
  UserOutlined,
  UnorderedListOutlined,
  AppstoreAddOutlined,
} from '@ant-design/icons';

const AddForm = ({ onSubmit, label }) => {
  const [value, setValue] = useState('');

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    //actionType(value, selectedUser);
    onSubmit(value);
    setValue('');
  };

  return (
    <Form onFinish={handleSubmit}>
      <div className='form-group'>
        <Form.Item
          rules={[{ required: true, message: 'Please input your username!' }]}>
          <Input
            size='large'
            prefix={
              label === 'Add user' ? (
                <UserOutlined />
              ) : (
                <UnorderedListOutlined />
              )
            }
            name='contentTask'
            onChange={handleChange}
            placeholder={`please ${label} here`}
          />
        </Form.Item>
      </div>

      <Button
        style={{
          backgroundColor: label === 'Add user' ? '#1890ff' : '#13c2c2',
          color: 'white',
        }}
        shape='round'
        onClick={handleSubmit}
        placeholder='add user'
        icon={
          label === 'Add user' ? <UserAddOutlined /> : <AppstoreAddOutlined />
        }>
        {label}
      </Button>
    </Form>
  );
};

export default AddForm;
