import React from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import * as selectors from '../redux/selectors';
import * as actionCreators from '../redux/actionCreators';
import { Select, Typography } from 'antd';
import { FormattedMessage } from 'react-intl';

const DisplayUsers = ({ users, selectUser }) => {
  return (
    <div>
      {users.length !== 0 ? (
        <div>
          <br />
          <Typography.Title level={3}><FormattedMessage id='titleSelectUser'/></Typography.Title>
          <Select
            style={{ width: '100%' }}
           
            onChange={(e) => selectUser(parseInt(e))}>
            <Select.Option value={null} > <FormattedMessage id='addUser'/></Select.Option>
            {users.map((user) => {
              return (
                <Select.Option key={user.userId} value={user.userId}>
                  {user.name}
                </Select.Option>
              );
            })}
          </Select>
        </div>
      ) : (
        false
      )}
    </div>
  );
};
const mapStateToProps = (users) => {
  return {
    users: selectors.getUsers(users),
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...actionCreators }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DisplayUsers);
