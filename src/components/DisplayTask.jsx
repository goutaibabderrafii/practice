import React from 'react';
import { List, Divider } from 'antd';

import { connect } from 'react-redux';
import * as selectors from '../redux/selectors';
import { FormattedMessage } from 'react-intl';

const DisplayTask = ({ tasks, selectedUser }) => {
  return (
    <div>
      {selectedUser ? <Divider><FormattedMessage id='taskTitle'/></Divider> : false}

      {tasks.length !== 0 ? (
        <List
          dataSource={tasks.filter((task) => task.userId === selectedUser)}
          renderItem={(item) => (
            <List.Item key={item.idTask} value={item.idTask}>
              {item.content}
            </List.Item>
          )}
        />
      ) : (
        false
      )}
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    tasks: selectors.getTasks(state),
    selectedUser: selectors.getSelectedUser(state),
  };
};

export default connect(mapStateToProps, null)(DisplayTask);
