import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';

import { bindActionCreators } from 'redux';

import * as actionCreators from '../redux/actionCreators';
import {
  HomeOutlined,
  ContactsOutlined,
  ContainerOutlined,
  AppstoreOutlined,
} from '@ant-design/icons';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

const Nav = ({ changeLanguage }) => {
  return (
    <Layout.Sider
      style={{
        overflow: 'auto',
        height: '100%',
        position: 'fixed',
        left: 0,
        backgroundColor: '#bae7ff',
      }}>
      <Menu style={{ backgroundColor: '#e6f7ff' }}>
        <Menu.Item key='1' icon={<HomeOutlined />}>
          <Link to='/'>
            <FormattedMessage id='home' />
          </Link>
        </Menu.Item>
        <Menu.Item key='2' icon={<ContainerOutlined />}>
          <Link to='/About'>
            <FormattedMessage id='aboutTitle' />
          </Link>
        </Menu.Item>
        <Menu.Item key='3' icon={<ContactsOutlined />}>
          <Link to='/ContactUs'>
            <FormattedMessage id='contactUs' />
          </Link>
        </Menu.Item>
        <Menu.SubMenu key='sub1' icon={<AppstoreOutlined />} title='languages'>
          <Menu.Item key='3' onClick={() => changeLanguage('en-us')}>
            English
          </Menu.Item>
          <Menu.Item key='4' onClick={() => changeLanguage('fr-fr')}>
            French
          </Menu.Item>
          <Menu.Item key='5' onClick={() => changeLanguage('de-de')}>
            GERMAN
          </Menu.Item>
        </Menu.SubMenu>
      </Menu>
    </Layout.Sider>
  );
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...actionCreators }, dispatch);

export default connect(null, mapDispatchToProps)(Nav);
