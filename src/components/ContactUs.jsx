import { Card, Row, Typography } from 'antd';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Input, Button } from 'antd';

const ContactUs = () => {
  return (
    <Row justify='center'>
      <Card.Grid>
        <Typography.Title level={2}>
          <FormattedMessage id='contactTitle' />
        </Typography.Title>
        <Form>
          <Form.Item
            label='First Name'
            rules={[
              {
                required: true,
                message: 'Please input your First Name!',
              },
            ]}>
            <Input />
          </Form.Item>

          <Form.Item
            label='Last Name'
            rules={[
              {
                required: true,
                message: 'Please input your Last Name!',
              },
            ]}>
            <Input />
          </Form.Item>
          <Form.Item
            label='Message'
            rules={[
              {
                required: true,
                message: 'Please input your Message!',
              },
            ]}>
            <Input.TextArea />
          </Form.Item>
        </Form>
        <Button type='primary' htmlType='submit'>
          Submit
        </Button>
      </Card.Grid>
    </Row>
  );
};

export default ContactUs;
