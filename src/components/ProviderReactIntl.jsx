import React from 'react';
import { IntlProvider } from 'react-intl';
import combineAllOFLanguagesMessages from '../i18n/languages/index.js';
import { connect } from 'react-redux';

import * as selectors from '../redux/selectors';

const ProviderReactIntl = ({ children, changeLanguage }) => {
  return (
    <IntlProvider
      locale={changeLanguage}
      messages={combineAllOFLanguagesMessages[changeLanguage]}>
      {children}
    </IntlProvider>
  );
};

const mapStateToProps = (state) => {
  return {
    changeLanguage: selectors.getLanguage(state),
  };
};
export default connect(mapStateToProps, null)(ProviderReactIntl);
