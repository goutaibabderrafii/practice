import React, { useEffect } from 'react';
import { Alert } from 'antd';

import { connect } from 'react-redux';
import * as selectors from '../redux/selectors';
import * as actionCreators from '../redux/actionCreators';
import { bindActionCreators } from 'redux';
const AlertMessage = ({ showAlert, showAlertData }) => {
  console.log(showAlert);
  useEffect(() => {
    setTimeout(() => {
      showAlert(false, '', '');
      }, 2000);
    }, [showAlert, showAlertData]);

  return (
    <Alert message={showAlertData.message} type={showAlertData.type} showIcon />
  );
};
const mapStateToProps = (state) => {
  return {
    showAlertData: selectors.getShowAlert(state),
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...actionCreators }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(AlertMessage);
