import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Card, Row, Typography } from 'antd';

/* const messageInFrench = {
  title: 'La magazine des développeurs',
  myMessage: `GeexReview est un magazine numérique trimestriel réalisé par des développeurs pour des développeurs. Couvrant dans chaque édition un thème informatique spécifique, l'objectif principal de ce magazine numérique est de diffuser la culture du codage et de
  partager les derniers sujets technologiques avec la communauté des développeurs et des passionnés d'informatique. GeexReview est ouvert à toute contribution, si vous avez du contenu pertinent, n'hésitez pas à nous contacter.
  `,
}; */

/* <IntlProvider messages={messageInFrench} locale='fr' defaultLocale='eng'> */
const About = () => {
  return (
    <Row justify='center'>
      <Card.Grid>
        <Typography.Title level={3}>
          <FormattedMessage id="title" />
        </Typography.Title>
        <Typography.Paragraph>
          <FormattedMessage id='about' />
        </Typography.Paragraph>
      </Card.Grid>
    </Row>
    /* </IntlProvider> */
  );
};

export default About;
