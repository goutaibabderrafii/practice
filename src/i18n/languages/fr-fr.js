import LOCALES from './locales';
const fr = {
  [LOCALES.FRENSH]: {
    appTitle: 'liste de choses à faire ',
    title: 'La magazine du développeur commence',
    taskTitle: 'Taches',
    titleSelectUser: 'Selectionner un utilisateur ',
    addUser: `Merci dajouter un utilisateur`,
    home: 'Acceuil',
    aboutTitle: 'A propos',
    contactUs: 'Contactez-nous ',
    
    contactTitle:'Contactez-nous',
    about: `GeexReview est un magazine numérique trimestriel réalisé par des développeurs pour des développeurs. Couvrant dans chaque édition un thème informatique spécifique, l'objectif principal de ce magazine numérique est de diffuser la culture du codage et de
      partager les derniers sujets technologiques avec la communauté des développeurs et des passionnés d'informatique. GeexReview est ouvert à toute contribution, si vous avez du contenu pertinent, n'hésitez pas à nous contacter.
      `,
  },
};
export default fr;
