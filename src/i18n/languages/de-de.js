import  LOCALES  from './locales';

export const de = {
  [LOCALES.GERMAN]: {
    
    appTitle:'Aufgabenliste',
    title: 'Der Entwickler magazinpm start',
    taskTitle: 'Aufgaben',
    titleSelectUser: 'Wählen Sie einen Benutzer',
    addUser: 'Bitte hinzufügen Benutzer',
    home : 'home',
    aboutTitle:'über',
    contactUs:'Kontakt ',
    
    contactTitle:'Kontakt',
    about:
      'GeexReview ist ein vierteljährlich erscheinendes, digitales Magazin von Entwicklern für Entwickler. In jeder Ausgabe wird ein bestimmtes IT-Thema behandelt. Das Hauptziel dieses digitalen Magazins ist es, die Programmierkultur zu verbreiten und die neuesten Technologiethemen mit der Gemeinschaft der Entwickler und IT-Enthusiasten zu teilen. GeexReview ist offen für jeden Beitrag, wenn Sie relevante Inhalte haben, können Sie uns gerne kontaktieren Übersetzt mit www.DeepL.com/Translator (kostenlose Version)📧',
  },
};
export default de ;