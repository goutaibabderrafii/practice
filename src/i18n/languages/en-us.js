import LOCALES from './locales';

const en = {
  [LOCALES.ENGLISH]: {
    appTitle:'To Do List',
    title: 'The developer magazinenpm start',
    taskTitle: 'Tasks',
    titleSelectUser: 'Select a user ',
    addUser: 'Please add User',
    home : 'Home',
    aboutTitle:'About',
    contactUs:'Contact us',
    contactTitle:'Contact Us',
    about:
      'GeexReview is a quarterly digital magazine made by developers fordevelopers. Covering in every edition a specific IT theme, the main purpose of this digital magazine is to spread the coding culture andshare the latest technology topics with the community of developersand IT enthusiasts. GeexReview is open for any contribution, if youhave any relevant content, feel free to get in touch 📧',
  },
};
export default en;
