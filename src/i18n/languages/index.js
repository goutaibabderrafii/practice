import en from './en-us';
import fr from './fr-fr';
import de from './de-de';

const combineAllOFLanguagesMessages = { ...en, ...fr, ...de };
export default combineAllOFLanguagesMessages;
