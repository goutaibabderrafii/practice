const LOCALES = {
  ENGLISH: 'en-us',
  FRENSH: 'fr-fr',
  GERMAN: 'de-de',
};

export default LOCALES;
