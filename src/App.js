import React from 'react';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Nav from './components/Nav';
import About from './components/About';
import ContactUs from './components/ContactUs';

function App() {
  return (
    <Router>
      <Nav />
      <br />
      <Switch>
        <Route exact path='/' component={Home}></Route>
        <Route exact path='/About' component={About}></Route>
        <Route exact path='/ContactUs' component={ContactUs}></Route>
      </Switch>
    </Router>
  );
}
export default App;
