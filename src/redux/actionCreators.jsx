import * as actionTypes from './actionTypes';

export const addUser = (username) => ({
  type: actionTypes.ADD_USER,
  payload: { username },
});
export const addUserSuccess = (addedUser) => ({
  type: actionTypes.ADD_USER_SUCCESS,
  payload: { addedUser },
});

export const addTask = (contentTask, selectedUser) => ({
  type: actionTypes.ADD_TASK,
  payload: { contentTask, selectedUser },
});
export const addTaskSuccess = (addTask) => ({
  type: actionTypes.ADD_TASK_SUCCESS,
  payload: { addTask },
});

export const selectUser = (userId) => ({
  type: actionTypes.SELECT_USER,
  payload: { userId },
});

export const showAlert = (showAlert, message, type) => ({
  type: actionTypes.SHOW_ALERT,
  payload: { showAlert, message, type },
});

export const changeLanguage = (language) => ({
  type: actionTypes.CHANGE_LANGUAGE,
  payload: { language },
});
