import { all, takeLatest, put, delay } from 'redux-saga/effects';

import * as localStorage from '../localStorage';

import * as actionCreators from '../actionCreators';
import * as actionTypes from '../actionTypes';

export function* addUserWorker({ payload }) {
  try {
    yield delay(2000);
    const addedUser = yield localStorage.saveUserTolocalStorage(
      payload.username
    );
    yield put(actionCreators.addUserSuccess(addedUser));
  } catch (error) {
    throw error;
  }
}
export function* addTaskWorker({ payload }) {
  try {
    yield delay(1000);
    const addedTask = yield localStorage.saveTaskTolocalStorage(
      payload.contentTask,
      payload.selectedUser
    );
    yield put(actionCreators.addTaskSuccess(addedTask));
  } catch (error) {
    throw error;
  }
}
export function* mainSaga() {
  yield all([
    takeLatest(actionTypes.ADD_USER, addUserWorker),
    takeLatest(actionTypes.ADD_TASK, addTaskWorker),
  ]);
}
