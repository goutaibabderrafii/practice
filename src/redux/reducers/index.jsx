import { combineReducers } from 'redux';
import users from './users';
import tasks from './tasks';
import selectedUser from './selectedUser';
import showAlert from './showAlert';
import changeLanguage from './changeLanguage'

const combinedReducers = combineReducers({
  users,
  tasks,
  selectedUser,
  showAlert,
  changeLanguage
});

export default combinedReducers;
