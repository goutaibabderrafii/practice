import * as actionTypes from '../actionTypes';

const selectedUser = (state = null, action) => {
  const { payload } = action;
  switch (action.type) {
    case actionTypes.SELECT_USER:
      return payload.userId;
    default:
      return state;
  }
};

export default selectedUser;
