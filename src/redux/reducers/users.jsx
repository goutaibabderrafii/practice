import * as actionTypes from '../actionTypes';

const users = (state = [], action) => {
  const users = state;
  const { payload } = action;
  switch (action.type) {
    case actionTypes.ADD_USER:
      console.log('loading posting user to the server');
      return state;

    case actionTypes.ADD_USER_SUCCESS:
      //const idUser = users.length + 1;
      console.log('user was successfuly added');
      return [...users, payload.addedUser];

    default:
      return state;
  }
};

export default users;
