import * as actionTypes from '../actionTypes';

const showAlert = (state = {}, action) => {
  const { payload } = action;

  switch (action.type) {
    case actionTypes.SHOW_ALERT:
      return {
        show: payload.showAlert,
        message: payload.message,
        type: payload.type,
      };
    default:
      return state;
  }
};

export default showAlert;
