import * as actionTypes from '../actionTypes';

const changeLanguage = (state = 'en-us', action) => {
  const { payload } = action;
  switch (action.type) {
    case actionTypes.CHANGE_LANGUAGE:
      return payload.language;
    default:
      return state;
  }
};

export default changeLanguage;
