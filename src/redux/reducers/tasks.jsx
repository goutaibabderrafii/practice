import * as actionTypes from '../actionTypes';

const tasks = (state = [], action) => {
  const tasks = state;
  const { payload } = action;
  switch (action.type) {
    case actionTypes.ADD_TASK:
      console.log('loading posting task to the server');
      return state;
    case actionTypes.ADD_TASK_SUCCESS:
      console.log('task was successfuly added');
      return [...tasks, payload.addTask];

    default:
      return state;
  }
};

export default tasks;
