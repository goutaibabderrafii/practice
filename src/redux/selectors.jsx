export const getUsers = (state) => {
  return state.users;
};
export const getTasks = (state) => {
  return state.tasks;
};
export const getSelectedUser = (state) => {
  return state.selectedUser;
};

export const getShowAlert = (state) => {
  return state.showAlert;
};
export const getLanguage = (state) => {
  return state.changeLanguage;
};
