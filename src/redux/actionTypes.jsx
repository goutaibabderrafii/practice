export const ADD_USER = '[user]:Add user';
export const ADD_USER_SUCCESS = '[user]:Add user success';
export const ADD_TASK_SUCCESS = '[user]:Add task success';
export const ADD_TASK = '[task]:Add task';
export const SELECT_USER = '[selectUser]:select user';
export const SHOW_ALERT = '[showAlert]:show alert';
export const CHANGE_LANGUAGE = '[changeLanguage]:change language';
