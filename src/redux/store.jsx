import { applyMiddleware, createStore, compose } from 'redux';
import combinedReducers from './reducers/index';
import createSagaMiddleware from 'redux-saga';
import { mainSaga } from './sagas/sagas';
import { loadState } from './localStorage';

 /* const initialState = {
  users: [],
  tasks: [],
  selectedUser: null,
  showAlert: { show: false, message: null, type: null },
};  */
const composeEnhancers =
  typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

const persistedState = loadState();
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  combinedReducers,
  persistedState,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);


sagaMiddleware.run(mainSaga);

export { store };
