const loadState = () => {
  try {
    return (
      JSON.parse(localStorage.getItem('state')) || {
        users: [],
        tasks: [],
      }
    );
  } catch (error) {
    throw error;
  }
};

/* const initialState = {
  users: [],
  tasks: [],
}; */

const saveUserTolocalStorage = (username) => {
  try {
    //const users = store.getState().users;
    const localStorageState = loadState();
    const { users } = localStorageState;

    console.log('user was successfuly added to localStorage');
    const addedUser = { userId: users.length + 1, name: username };
    const updateState = {
      ...localStorageState,
      users: [...users, addedUser],
    };

    window.localStorage.setItem('state', JSON.stringify(updateState));
    return addedUser;
  } catch (error) {
    throw error;
  }
};

const saveTaskTolocalStorage = (contentTask, selectedUser) => {
  try {
    const localStorageStatel = loadState();
    const { tasks } = localStorageStatel;
    const idTask = tasks.length + 1;

    console.log('task was successfuly added to localStorage');
    const addedTask = {
      idTask: idTask,
      userId: selectedUser,
      content: contentTask,
    };
    const updateState = {
      ...localStorageStatel,
      tasks: [...tasks, addedTask],
    };

    window.localStorage.setItem('state', JSON.stringify(updateState));
    return addedTask;
  } catch (error) {
    throw error;
  }
};

export { saveUserTolocalStorage, saveTaskTolocalStorage, loadState };
