import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'antd/dist/antd.css';
import reportWebVitals from './reportWebVitals';
import StoreProvider from './redux/storeProvider.jsx';
import ProviderReactIntl from './components/ProviderReactIntl';

ReactDOM.render(
  <React.StrictMode>
    <StoreProvider>
      <ProviderReactIntl  >
        <App />
      </ProviderReactIntl>
    </StoreProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
