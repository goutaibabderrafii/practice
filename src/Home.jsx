import AddForm from './components/AddForm';
import DisplayUsers from './components/DisplayUsers';
import DisplayTask from './components/DisplayTask';
import * as selectors from './redux/selectors';
import AlertMessage from './components/AlertMessage';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card, Row, Typography } from 'antd';

import * as actionCreators from './redux/actionCreators';
import { FormattedMessage } from 'react-intl';

const Home = ({ selectedUser, showAlert, addUser, addTask, showAlertData }) => {
  const handleUserSubmit = (content) => {
    if (!content) {
      alert('Please add user ');
      return;
    } else {
      addUser(content);
      showAlert(true, `${content} added`, 'info');
    }
  };
  const handleTaskSubmit = (content) => {
    if (!content) {
      alert('Please add task ');
      return;
    } else if (!selectedUser) {
      alert('Please select User ');
      return;
    } else {
      addTask(content, selectedUser);
      showAlert(true, 'task added', 'success');
    }
  };

  return (
    <Row justify='center'>
      <Card.Grid>
        <Typography.Title level={2}>
          <FormattedMessage id='appTitle' />
        </Typography.Title>

        <AddForm onSubmit={handleUserSubmit} label='Add user' />

        <DisplayUsers />
        <hr />

        <AddForm onSubmit={handleTaskSubmit} label='Add task' />
        <DisplayTask />

        <br />
        {showAlertData.show ? <AlertMessage timeOut={2000} /> : false}
      </Card.Grid>
    </Row>
  );
};
const mapStateToProps = (state) => {
  return {
    showAlertData: selectors.getShowAlert(state),
    selectedUser: selectors.getSelectedUser(state),
  };
};
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...actionCreators }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Home);
